#!/bin/bash
set -e # If any command fails, stop execution with that error

# Step 0 Check if root is running the script
if [ "$(id -u)" != "0" ]; then
	echo "Sorry, you are not root."
	exit 1
fi

# Step 1: Check for user approval
echo -e "\e[1;37mThe following software will be installed:\e[0m \e[1;32mgradle gcc cMake gdb clang build-essential git\e[0m"

read -p "Do you wish to continue? Yes [y] : No [n] Defaults to Yes " -n 4 -r RESPONSE

if [[ "${RESPONSE// }" =~ [yY](es)* ]] || [[ -z "${RESPONSE// }" ]]
 then
    
    #Step 1: Update package list
    echo -e "\e[1;36mUpdating Package List\n\e[0m" 
    apt update
	
    #Step 2 Install Basic Software
    echo -e "\e[1;36mInstalling CLion Pre-requisits\n\e[0m"
    apt -y install cmake gcc clang gdb valgrind build-essential # assume yes to all questions

elif [[ "${RESPONSE// }" =~ [nN](o)* ]]
 then
   echo -e "\e[0;32mYou could change your mind and run this script again at any time.\e[0m"
else
    #Printing error message
    echo -e "\e[0;31mInvalid inptu!\e[0m"
    exit 1
fi
